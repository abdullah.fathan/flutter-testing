import 'package:flutter_test/flutter_test.dart';
import 'package:testing_app/utils/price_utils.dart';

// void main() {
//   //simple uni testing
//   test("Addtion function test", () {
//     int price = 100000;

//     var discount = PriceUtils.discountPrice(price);

//     expect(discount, 90000);
//   });
// }

void main() {
  group('testing price util', () {
    test('testing harga diskon 1', () {
      int price = 1000000;

      var discount = PriceUtils.discountPrice(price);

      expect(discount, 900000);
    });
    test('testing harga diskon 2', () {
      int price = 100000;

      var discount = PriceUtils.discountPrice(price);

      expect(discount, 90000);
    });
  });
}
