import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:testing_app/screens/login_screen.dart';

void main() {
  //simple testing widget
  testWidgets("have one email text flied", (widgetTester) async {
    await widgetTester.pumpWidget(const MaterialApp(
      home: LoginScreen(),
    ));

    Finder emailTextFlied = find.byKey(const ValueKey('email'));

    expect(emailTextFlied, findsOneWidget);
  });
}
