import 'package:dio/dio.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:testing_app/models/books_model.dart';
import 'package:testing_app/services/books_services.dart';

import 'books_services_test.mocks.dart';

@GenerateMocks([Dio])
void main() {
  late MockDio mockDio;

  setUp(() {
    mockDio = MockDio();
  });

  group("Fetch API books", () {
    test("Should return list of books for success call", () async {
      // Mocking the response
      when(mockDio.get(fetchBooksURL)).thenAnswer(
        (_) async => Response(
          data: '[{"name":"fathan", "auther":"abdullah"}]',
          statusCode: 200,
          requestOptions: RequestOptions(path: fetchBooksURL),
        ),
      );

      // Calling the fetchBooks function with mockDio
      final books = await fetchBooks(mockDio);

      // Expectations
      expect(books, isA<List<BooksListModel>>());
    });

    test('throws an exception if the http call completes with an error',
        () async {
      // Mocking the response with an error
      when(mockDio.get(fetchBooksURL)).thenAnswer(
        (_) async => Response(
          data: 'Not Found',
          statusCode: 404,
          requestOptions: RequestOptions(path: fetchBooksURL),
        ),
      );

      // Expecting an exception
      expect(fetchBooks(mockDio), throwsException);
    });
  });
}
