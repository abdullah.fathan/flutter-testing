import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:testing_app/models/books_model.dart';

String fetchBooksURL =
    'https://raw.githubusercontent.com/Richa0305/mock-json/main/book.json';

Future<List<BooksListModel>> fetchBooks(Dio dio) async {
  final response = await dio.get(fetchBooksURL); // Call API

  if (response.statusCode == 200) {
    return List<BooksListModel>.from(
        json.decode(response.data).map((x) => BooksListModel.fromJson(x)));
  } else {
    throw Exception('Failed to load album');
  }
}
