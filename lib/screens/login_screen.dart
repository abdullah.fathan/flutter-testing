import 'package:flutter/material.dart';
import 'package:testing_app/screens/home_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailController = TextEditingController();
  final passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Login"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16),
        child: Center(
          child: Column(
            children: [
              TextField(
                controller: emailController,
                key: const ValueKey(
                  'email',
                ),
                decoration: const InputDecoration(
                  hintText: "Email",
                ),
              ),
              TextField(
                controller: emailController,
                key: const ValueKey(
                  'password',
                ),
                decoration: const InputDecoration(
                  hintText: "password",
                ),
              ),
              ElevatedButton(
                key: const ValueKey('login'),
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (_) => const HomeScreen())),
                child: const Text("Login"),
              )
            ],
          ),
        ),
      ),
    );
  }
}
