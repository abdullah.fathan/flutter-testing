import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:testing_app/services/books_services.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Home Screen'),
      ),
      body: FutureBuilder(
        future: fetchBooks(Dio()),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasData) {
            return ListView.separated(
              itemBuilder: (context, index) {
                var data = snapshot.data?[index];
                return ListTile(
                  title: Text('${data?.name}'),
                  subtitle: Text('${data?.decription}'),
                );
              },
              separatorBuilder: (context, index) {
                return const SizedBox(
                  height: 8,
                );
              },
              itemCount: snapshot.data?.length ?? 0,
            );
          }
          return const SizedBox();
        },
      ),
    );
  }
}
