class PriceUtils {
  //mendapatkan harga setelah mendapatkan diskon 10%
  static int discountPrice(int price) {
    double discount = price - (price * 0.1);
    return discount.toInt();
  }
}
