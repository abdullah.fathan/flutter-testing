class BooksListModel {
  int? id;
  String? name;
  String? auther;
  String? decription;
  String? amazon;

  BooksListModel({
    this.id,
    this.name,
    this.auther,
    this.decription,
    this.amazon,
  });

  factory BooksListModel.fromJson(Map<String, dynamic> json) => BooksListModel(
        id: json["id"],
        name: json["name"],
        auther: json["auther"],
        decription: json["decription"],
        amazon: json["amazon"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "auther": auther,
        "decription": decription,
        "amazon": amazon,
      };
}
