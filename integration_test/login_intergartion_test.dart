import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';
import 'package:testing_app/main.dart'; // Pastikan file ini mengimpor widget MyApp yang memulai aplikasi.

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('Login to HomeScreen test', (WidgetTester tester) async {
    // Jalankan aplikasi
    await tester.pumpWidget(const MyApp());

    // Temukan TextField untuk email dan masukkan email
    final emailField = find.byKey(const ValueKey('email'));
    await tester.enterText(emailField, 'test@example.com');

    // Temukan TextField untuk password dan masukkan password
    final passwordField = find.byKey(const ValueKey('password'));
    await tester.enterText(passwordField, 'password123');

    // Temukan tombol login dan tekan
    final loginButton = find.byKey(const ValueKey('login'));
    await tester.tap(loginButton);

    // Rebuild the widget setelah aksi di atas
    await tester.pumpAndSettle();

    // Verifikasi apakah kita berada di HomeScreen dengan memeriksa AppBar
    expect(find.text('Home Screen'), findsOneWidget);
  });
}
